#+title: XENSPACE.NET dotfiles
#+author: AlexHG @ xenspace.net

The official [[xenspace.net/projects?nav=dotfiles][xenspace]] UNIX environment setup

* About the xenspace workstation setup
  These configuration files turn a vanilla UNIX setup into the official xenspace development environment.
  Though they are primarily used and developed for the [[https://www.archlinux.org][arch]] Linux distribution they are compatible with all
  UNIX systems and even Windows machines with a few, simple tweaks. Note that this setup
  emphasizes performance and flexibility over flashiness and secondary features such as desktop effects.

  This workstation aims to provide the most basic toolset required to develop all other [[https://xenspace.net/projects][xenspace.net]]
  and a solid foundation for general desktop use.

* System requirements
  Any UNIX system will do, the newer the better. Most configurations work with Windows if their respective software runs on it.
  However, it is primarily built in and for [[https://www.archlinux.org/][arch]]  based systems to keep the main branch as updated as the software dependencies.

* Screenshots
  [[https://imgur.com/a/eQ1SkYn][imgur post]]
  #+html: <a href="https://ibb.co/qCjWjPD"><img src="https://i.ibb.co/PMj9jfr/desktop.jpg" alt="desktop" border="0" style="width:100%;"></a>
 
* Installation
  Visit [[https://xenspace.net/projects/?nav=dotfiles#4][xenspace.net]] for the step by step installation guide.
