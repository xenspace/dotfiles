;;           _______  _        _______  _______  _______  _______  _______     _        _______ _________
;; |\     /|(  ____ \( (    /|(  ____ \(  ____ )(  ___  )(  ____ \(  ____ \   ( (    /|(  ____ \\__   __/
;; ( \   / )| (    \/|  \  ( || (    \/| (    )|| (   ) || (    \/| (    \/   |  \  ( || (    \/   ) (
;;  \ (_) / | (__    |   \ | || (_____ | (____)|| (___) || |      | (__       |   \ | || (__       | |
;;   ) _ (  |  __)   | (\ \) |(_____  )|  _____)|  ___  || |      |  __)      | (\ \) ||  __)      | |
;;  / ( ) \ | (      | | \   |      ) || (      | (   ) || |      | (         | | \   || (         | |
;; ( /   \ )| (____/\| )  \  |/\____) || )      | )   ( || (____/\| (____/\ _ | )  \  || (____/\   | |
;; |/     \|(_______/|/    )_)\_______)|/       |/     \|(_______/(_______/(_)|/    )_)(_______/   )_(
;; Author: AlexHG @ xenspace.net
;; License: MIT. Use at your own risk.
                                                   

;; Include all your packages in the list to have them automatically downloaded, validated
;; and updated
(load "~/.emacs.d/my-packages.el")

;; MODULE LOADING
;; Keep everything relevant in the same file. This greatly
;; aids in debugging and keeping track of what you actually
;; use.
(require 'interface-conf)
(require 'helm-conf)
(require 'flycheck-conf)
(require 'company-conf)
(require 'yasnippet-conf)
(require 'asm-conf)
(require 'web-conf)
(require 'lua-conf)
(require 'python-conf)
(require 'c-conf)
(require 'keys-conf)
(require 'general-conf)

;; User details
(setq user-full-name "AlexHG")
(setq user-mail-address "xenspace@protonmail.com")

;; Warning level
;; Careful, if you completely disable them you may end up making an otherwise simple debugging
;; process a complete nightmare
(setq warning-minimum-level :emergency)

;; Backup files
;; Keep them organized
(setq backup-directory-alist `(("." . "~/.emacs.d/backup_files")))
(setq backup-by-copying t)
(setq delete-old-versions t
  kept-new-versions 6
  kept-old-versions 2
  version-control t)
;; Or disable them (at your own risk!)
;; (setq make-backup-files nil)

;; Automatically recompile everything that may need it
;; KEEPING THIS ENABLED SLOWS DOWN INITIALIZATION
(byte-recompile-directory (expand-file-name "~/.emacs.d") 0)

;;END CUSTOM EMACS CONFIGURATION

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
 [default bold shadow italic underline bold bold-italic bold])
 '(ansi-color-names-vector
 ["#454545" "#cd5542" "#6aaf50" "#baba36" "#5180b3" "#ab75c3" "#68a5e9" "#bdbdb3"])
 '(custom-enabled-themes (quote (dracula)))
 '(custom-safe-themes
 (quote
  ("45489fbd8535a88dbeb948981263499975218b09063c9562e0afb32e581fb971" "5f1bd7f67dc1598977e69c6a0aed3c926f49581fdf395a6246f9bc1df86cb030" "36ca8f60565af20ef4f30783aa16a26d96c02df7b4e54e9900a5138fb33808da" "c9ddf33b383e74dac7690255dd2c3dfa1961a8e8a1d20e401c6572febef61045" "7153b82e50b6f7452b4519097f880d968a6eaf6f6ef38cc45a144958e553fbc6" "5e3fc08bcadce4c6785fc49be686a4a82a356db569f55d411258984e952f194a" "a0feb1322de9e26a4d209d1cfa236deaf64662bb604fa513cca6a057ddf0ef64" "ab04c00a7e48ad784b52f34aa6bfa1e80d0c3fcacc50e1189af3651013eb0d58" "2540689fd0bc5d74c4682764ff6c94057ba8061a98be5dd21116bf7bf301acfb" "d8dc153c58354d612b2576fea87fe676a3a5d43bcc71170c62ddde4a1ad9e1fb" "3a5f04a517096b08b08ef39db6d12bd55c04ed3d43b344cf8bd855bde6d3a1ae" "5a21604c4b1f2df98e67cda2347b8f42dc9ce471a48164fcb8d3d52c3a0d10be" "bf798e9e8ff00d4bf2512597f36e5a135ce48e477ce88a0764cfb5d8104e8163" "04dd0236a367865e591927a3810f178e8d33c372ad5bfef48b5ce90d4b476481" "7356632cebc6a11a87bc5fcffaa49bae528026a78637acd03cae57c091afd9b9" "20bf9f519f78b247da9ccf974c31d3537bee613ff11579f539b2781246dee73b" "b583823b9ee1573074e7cbfd63623fe844030d911e9279a7c8a5d16de7df0ed0" default)))
 '(diary-entry-marker (quote font-lock-variable-name-face))
 '(emms-mode-line-icon-image-cache
 (quote
  (image :type xpm :ascent center :data "/* XPM */
static char *note[] = {
/* width height num_colors chars_per_pixel */
\"    10   11        2            1\",
/* colors */
\". c #1ba1a1\",
\"# c None s None\",
/* pixels */
\"###...####\",
\"###.#...##\",
\"###.###...\",
\"###.#####.\",
\"###.#####.\",
\"#...#####.\",
\"....#####.\",
\"#..######.\",
\"#######...\",
\"######....\",
\"#######..#\" };")))
 '(fci-rule-color "#404040")
 '(gnus-logo-colors (quote ("#4c8383" "#bababa")) t)
 '(gnus-mode-line-image-cache
 (quote
  (image :type xpm :ascent center :data "/* XPM */
static char *gnus-pointer[] = {
/* width height num_colors chars_per_pixel */
\"    18    13        2            1\",
/* colors */
\". c #1ba1a1\",
\"# c None s None\",
/* pixels */
\"##################\",
\"######..##..######\",
\"#####........#####\",
\"#.##.##..##...####\",
\"#...####.###...##.\",
\"#..###.######.....\",
\"#####.########...#\",
\"###########.######\",
\"####.###.#..######\",
\"######..###.######\",
\"###....####.######\",
\"###..######.######\",
\"###########.######\" };")) t)
 '(package-selected-packages
 (quote
  (latex-preview-pane markdown-mode auctex goto-chg yaml-mode dumb-jump org-journal powerline majapahit-theme browse-kill-ring nlinum move-text mode-icons symon expand-region restclient drag-stuff zencoding-mode rainbow-delimiters rainbow-mode highlight-symbol highlight-numbers diff-hl vlf neotree ztree recentf-ext clean-aindent-mode duplicate-thing volatile-highlights undo-tree smartparens ws-butler x86-lookup nasm-mode iasm-mode py-autopep8 elpy xref-js2 js2-refactor js2-mode web-mode php-extras ac-php function-args yasnippet-snippets yasnippet flycheck-tip flycheck-irony flycheck company-glsl company-arduino company-jedi company-anaconda company-lua company-irony-c-headers company-irony company-web company-tern company-php company helm-tramp helm-themes helm-gtags helm)))
 '(pos-tip-background-color "#303030")
 '(red "#ffffff")
 '(vc-annotate-background "#404040")
 '(vc-annotate-color-map
 (quote
  ((20 . "#ea4141")
   (40 . "#db4334")
   (60 . "#e9e953")
   (80 . "#c9d617")
   (100 . "#dc7700")
   (120 . "#bcaa00")
   (140 . "#29b029")
   (160 . "#47cd57")
   (180 . "#60a060")
   (200 . "#319448")
   (220 . "#078607")
   (240 . "#1ec1c4")
   (260 . "#1ba1a1")
   (280 . "#26d5d5")
   (300 . "#58b1f3")
   (320 . "#00a2f5")
   (340 . "#1e7bda")
   (360 . "#da26ce"))))
 '(vc-annotate-very-old-color "#da26ce")
 '(xterm-color-names
 ["#303030" "#D66F84" "#D79887" "#D49A8A" "#94B1A3" "#A8938C" "#989584" "#BAB2A9"])
 '(xterm-color-names-bright
 ["#3A3A3A" "#E47386" "#CC816B" "#769188" "#7D6F6A" "#9C8772" "#BAB2A9"]))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
