;;           _______  _        _______  _______  _______  _______  _______     _        _______ _________
;; |\     /|(  ____ \( (    /|(  ____ \(  ____ )(  ___  )(  ____ \(  ____ \   ( (    /|(  ____ \\__   __/
;; ( \   / )| (    \/|  \  ( || (    \/| (    )|| (   ) || (    \/| (    \/   |  \  ( || (    \/   ) (
;;  \ (_) / | (__    |   \ | || (_____ | (____)|| (___) || |      | (__       |   \ | || (__       | |
;;   ) _ (  |  __)   | (\ \) |(_____  )|  _____)|  ___  || |      |  __)      | (\ \) ||  __)      | |
;;  / ( ) \ | (      | | \   |      ) || (      | (   ) || |      | (         | | \   || (         | |
;; ( /   \ )| (____/\| )  \  |/\____) || )      | )   ( || (____/\| (____/\ _ | )  \  || (____/\   | |
;; |/     \|(_______/|/    )_)\_______)|/       |/     \|(_______/(_______/(_)|/    )_)(_______/   )_(
;; Author: AlexHG @ xenspace.net
;; License: MIT. Use at your own risk.

;; COMPANY MODE SETTINGS !!! LOAD ORDER MATTERS !!!
;; ------------------------------------------------------------------------------------------------------------------------------------ ;;
;; NOTE ABOUT COMPANY MODES: THE ORDER IN WHICH YOU LOAD THE MODES MATTERS, EXPERIMENT IF SOMETHING GOES WRONG WITH M-X COMPANY-DIAG    ;;
;; ------------------------------------------------------------------------------------------------------------------------------------ ;;

(require 'company)
(add-hook 'after-init-hook 'global-company-mode)

(setq company-show-numbers t
      company-minimum-prefix-length 3
      company-tooltip-limit 30
      company-idle-delay 0.2)

(setq company-auto-complete t)
(setq company-tooltip-align-annotations t)

;;HTML
(require 'company-web-html)                          ; load company mode html backend
(add-to-list 'company-backends 'company-web-html)

;;PHP
;; PHP auto completion depends on php-extras generated library
;; M-x load-library RET php-extras-gen-eldoc RET
;; M-x php-extras-generate-eldoc RET
;; For local auto complete add a .ac-php-conf.json file to the project root
;; then run ac-php-remake-tags to generate a template, then configure it
(add-hook 'php-mode-hook
          '(lambda ()
             (require 'company-php)
             (company-mode t)
             (set (make-local-variable 'company-backends)
                   '((php-extras-company company-dabbrev-code) company-capf company-files))))


;;emoji
;; (require 'company-emoji)
;; (add-to-list 'company-backends 'company-emoji)

;;LUA
(require 'company-lua)
(add-to-list 'company-backends 'company-lua)

;;JS
(require 'company-tern)
(add-to-list 'company-backends 'company-tern)
(add-hook 'js2-mode-hook (lambda ()
                           (tern-mode)
                           (company-mode)))
;; Disable completion keybindings, as we use xref-js2 instead
(define-key tern-mode-keymap (kbd "M-.") nil)
(define-key tern-mode-keymap (kbd "M-,") nil)


;;PYTHON
(defun my/python-mode-hook ()
  (add-to-list 'company-backends 'company-jedi))

(add-hook 'python-mode-hook 'my/python-mode-hook)
;; (add-to-list 'company-backends 'company-jedi)
;; (add-to-list 'company-backends 'company-anaconda)

;;HTML
;; (require 'company-web-html)                          ; load company mode html backend
;; (add-to-list 'company-backends 'company-web-html)

;;C / C++ headers
(require 'company-irony)
(require 'company-irony-c-headers)
;; (require 'company-c-headers)
;; (add-to-list 'company-backends 'company-c-headers)
;;(add-to-list 'company-backends 'company-irony)
(add-to-list 'company-backends '(company-irony-c-headers company-irony))

;; (add-to-list 'company-c-headers-path-system "/usr/include/c++/9.2.0/")
;; (add-to-list 'company-c-headers-path-system "/usr/include/c++/9.2.0/")
;; (add-to-list 'company-c-headers-path-system "/usr/include/boost/")
;; (add-to-list 'company-c-headers-path-system "/home/alexhg/Qt/5.12.6/gcc_64/include")

;; If you installed this package from without MELPA, you may need
(require 'company-arduino)
;; Configuration for irony.el
;; Add arduino's include options to irony-mode's variable.
(add-hook 'irony-mode-hook 'company-arduino-turn-on)
;; Configuration for company-c-headers.el
;; The `company-arduino-append-include-dirs' function appends
;; Arduino's include directories to the default directories
;; if `default-directory' is inside `company-arduino-home'. Otherwise
;; just returns the default directories.
;; Please change the default include directories accordingly.
(defun my-company-c-headers-get-system-path ()
  "Return the system include path for the current buffer."
  (let ((default '("/usr/include/" "/usr/local/include/")))
    (company-arduino-append-include-dirs default t)))
(setq company-c-headers-path-system 'my-company-c-headers-get-system-path)

;; Activate irony-mode on arduino-mode
(add-hook 'arduino-mode-hook 'irony-mode)

;; GLSL completion
;; requires glslang https://github.com/KhronosGroup/glslang
;; (require'company-glsl)
;; (add-to-list 'company-backends 'company-glsl)

(provide 'company-conf)
