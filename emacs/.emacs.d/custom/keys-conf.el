;;           _______  _        _______  _______  _______  _______  _______     _        _______ _________
;; |\     /|(  ____ \( (    /|(  ____ \(  ____ )(  ___  )(  ____ \(  ____ \   ( (    /|(  ____ \\__   __/
;; ( \   / )| (    \/|  \  ( || (    \/| (    )|| (   ) || (    \/| (    \/   |  \  ( || (    \/   ) (
;;  \ (_) / | (__    |   \ | || (_____ | (____)|| (___) || |      | (__       |   \ | || (__       | |
;;   ) _ (  |  __)   | (\ \) |(_____  )|  _____)|  ___  || |      |  __)      | (\ \) ||  __)      | |
;;  / ( ) \ | (      | | \   |      ) || (      | (   ) || |      | (         | | \   || (         | |
;; ( /   \ )| (____/\| )  \  |/\____) || )      | )   ( || (____/\| (____/\ _ | )  \  || (____/\   | |
;; |/     \|(_______/|/    )_)\_______)|/       |/     \|(_______/(_______/(_)|/    )_)(_______/   )_(
;; Author: AlexHG @ xenspace.net
;; License: MIT. Use at your own risk.

;; HOTKEY THAT DON'T REQUIRE EXTRA PACKAGES


;;Resize window keybindings
(global-set-key (kbd "S-C-M-<left>") 'shrink-window-horizontally)
(global-set-key (kbd "S-C-M-<right>") 'enlarge-window-horizontally)
(global-set-key (kbd "S-C-M-<up>") 'shrink-window)
(global-set-key (kbd "S-C-M-<down>") 'enlarge-window)

;;Rename
(global-set-key (kbd "C-c r")  'vc-rename-file)

;;Disable line highlighting
(global-set-key (kbd "C-c l") 'hl-line-mode)

;;window navidation in keybindings.el
(global-set-key (kbd "C-x <up>") 'windmove-up)
(global-set-key (kbd "C-x <down>") 'windmove-down)
(global-set-key (kbd "C-x <left>") 'windmove-left)
(global-set-key (kbd "C-x <right>") 'windmove-right)

(provide 'keys-conf)
