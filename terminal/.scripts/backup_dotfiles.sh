#           _______  _        _______  _______  _______  _______  _______     _        _______ _________
# |\     /|(  ____ \( (    /|(  ____ \(  ____ )(  ___  )(  ____ \(  ____ \   ( (    /|(  ____ \\__   __/
# ( \   / )| (    \/|  \  ( || (    \/| (    )|| (   ) || (    \/| (    \/   |  \  ( || (    \/   ) (
#  \ (_) / | (__    |   \ | || (_____ | (____)|| (___) || |      | (__       |   \ | || (__       | |
#   ) _ (  |  __)   | (\ \) |(_____  )|  _____)|  ___  || |      |  __)      | (\ \) ||  __)      | |
#  / ( ) \ | (      | | \   |      ) || (      | (   ) || |      | (         | | \   || (         | |
# ( /   \ )| (____/\| )  \  |/\____) || )      | )   ( || (____/\| (____/\ _ | )  \  || (____/\   | |
# |/     \|(_______/|/    )_)\_______)|/       |/     \|(_______/(_______/(_)|/    )_)(_______/   )_(
# Author: AlexHG @ xenspace.net
# License: MIT. Use at your own risk.


#!/bin/bash

set +e

# CHANGE TO YOUR OWN GIT CLONE DIR
backup_dir="/home/$USER/Documents/repos/dotfiles/"

echo "Backing data to: ${backup_dir}"
echo "Creating folders..."

# MAKE DIRECTORIES
mkdir -p "${backup_dir}"
mkdir -p "${backup_dir}/emacs/"
mkdir -p "${backup_dir}/emacs/.emacs.d"
mkdir -p "${backup_dir}/terminal/"
mkdir -p "${backup_dir}/terminal/fonts/"

cd "${backup_dir}"
echo "Backing files up..."

# EMACS
echo "EMACS..."
cp ~/.emacs "${backup_dir}/emacs/"
cp ~/.emacs.d/my-packages.el "${backup_dir}/emacs/.emacs.d"
cp ~/.emacs.d/x86-lookup-pdf.pdf "${backup_dir}/emacs/.emacs.d"
cp -r ~/.emacs.d/custom/ "${backup_dir}/emacs/.emacs.d"
cp -r ~/.emacs.d/irony/ "${backup_dir}/emacs/.emacs.d"
cp -r ~/.emacs.d/semanticdb/ "${backup_dir}/emacs/.emacs.d"
cp -r ~/.emacs.d/snippets/ "${backup_dir}/emacs/.emacs.d"
# CLEAN AUTO GENERATED FILES
cd "${backup_dir}/emacs/.emacs.d/custom/"
# Delete temporary files
find . -name "*.elc" -delete
find . -name "*.el#" -delete
cd "${backup_dir}"



# AWESOME WM
echo "AwesomeWM..."
cp -r ~/.config/awesome/ "$backup_dir/"

# TERMINAL AND COLORS
echo "Terminal related files..."
cp ~/.Xresources "${backup_dir}/terminal/"
cp ~/.tmux.conf  "${backup_dir}/terminal/"
cp -r ~/.tmux/  "${backup_dir}/terminal/"
cp ~/.zshrc  "${backup_dir}/terminal/"
cp -r ~/.urxvt/  "${backup_dir}/terminal/"
cp ~/.weechat/weechat.conf  "${backup_dir}/terminal/"

cp -r ~/.fonts/ProFontWindows.ttf  "${backup_dir}/terminal/fonts/"
cp -r ~/.fonts/TerminalVector.ttf  "${backup_dir}/terminal/fonts/"
cp -r ~/.scripts/ "${backup_dir}/terminal/"

# NCMPCPP
echo "MPD & NCMPCPP..."
cp -r ~/.ncmpcpp/  "${backup_dir}/terminal/"
rm "${backup_dir}/terminal/.ncmpcpp/error.log"

# PERSONAL FILES
# Remember to change with your gpg key data
# echo "Copying personal files..."
# cd "${backup_dir}"
# cp -v -r ~/.mutt/  "${backup_dir}/private/"
# cp -v -r ~/.muttrc  "${backup_dir}/private/"
# cp -v -r ~/Documents/journal/ "${backup_dir}/private/"
# echo "Encrypting..."
# # Turn directory into a file, use "tar xzf filename.tar.gz" to revert
# tar czf private.tar.gz private/
# # zip -r private.zip "${backup_dir}/private/"
# gpg --recipient alexhg.eng@protonmail.com --encrypt -e private.tar.gz
# echo "Cleaning up..."
# rm -r private/
# mv private.tar.gz.gpg ???
# rm private.tar.gz

cd $backup_dir
inside_git_repo="$(git rev-parse --is-inside-work-tree 2>/dev/null)"

if [ "$inside_git_repo" ]; then
    echo "Git repository found."
    echo "Updating local git repository..."
    git add .
    # git commit -m "$(date +%F_%T)"
    git commit
    git checkout
else
    echo "Git repository NOT found."
    echo "Creating a new git repository..."
    echo "Updating new local git repository..."
    touch "${backup_dir}/README.org"
    touch "${backup_dir}/CHANGELOG.org"
    git init
    git add .
    # git commit -m "$(date +%F_%T)"
    git commit
    git checkout
fi


echo "Backup complete."

