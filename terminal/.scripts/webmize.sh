#           _______  _        _______  _______  _______  _______  _______     _        _______ _________
# |\     /|(  ____ \( (    /|(  ____ \(  ____ )(  ___  )(  ____ \(  ____ \   ( (    /|(  ____ \\__   __/
# ( \   / )| (    \/|  \  ( || (    \/| (    )|| (   ) || (    \/| (    \/   |  \  ( || (    \/   ) (
#  \ (_) / | (__    |   \ | || (_____ | (____)|| (___) || |      | (__       |   \ | || (__       | |
#   ) _ (  |  __)   | (\ \) |(_____  )|  _____)|  ___  || |      |  __)      | (\ \) ||  __)      | |
#  / ( ) \ | (      | | \   |      ) || (      | (   ) || |      | (         | | \   || (         | |
# ( /   \ )| (____/\| )  \  |/\____) || )      | )   ( || (____/\| (____/\ _ | )  \  || (____/\   | |
# |/     \|(_______/|/    )_)\_______)|/       |/     \|(_______/(_______/(_)|/    )_)(_______/   )_(
# Author: AlexHG @ xenspace.net
# License: MIT. Use at your own risk.


#!/bin/bash
file_path="$1"
file_dest="$2"
video_format="mp4"
output_format="webm"
echo "---------------------------"
echo "MP4 to convert: $file_path"
echo "WEBM: $file_dest"
echo "---------------------------"
if [ ! -f "$file_path" ]; then
    echo "Source file doesn't exist. (It must have an .mp4 extension)."
    exit -1
fi

if [ -f "$file_dest" ]; then
    echo "Destination file already axists."
    exit -2
fi

filename=$(basename "$file_path")
extension="${filename##*.}"
filename="${filename%.*}"

output_filename=$(basename "$file_dest")
output_extension="${output_filename##*.}"

if [ "$output_extension" != "$output_format" ]; then
    echo "Invalid destination."
    exit -3
fi

# echo $filename
# echo $extension
# echo $filename

# echo $output_filename
# echo $output_extension

# Equality Comparison
if [ "$extension" == "$video_format" ]; then
    ffmpeg -i "$file_path" -c:v libvpx -b:v 1M -c:a libvorbis "$file_dest"
else
    echo "Source file must have a .mp4 extension!"
fi
